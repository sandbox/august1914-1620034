<?php

/**
 * @file
 * Example tpl file for theming a single aggitem-specific theme
 *
 * Available variables:
 * - $status: The variable to theme (while only show if you tick status)
 * 
 * Helper variables:
 * - $aggitem: The AggItem object this status is derived from
 */
?>

<div class="aggitem-status">
  <?php print '<strong>AggItem Sample Data:</strong> ' . $aggitem_sample_data = ($aggitem_sample_data) ? 'Switch On' : 'Switch Off' ?>
</div>