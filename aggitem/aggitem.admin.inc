<?php

/**
 * @file
 * AggItem editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class AggItemUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    
    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'AggItems',
      'description' => 'Add edit and update aggitems.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );
    
    // Change the overview menu type for the list of aggitems.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    
    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => 'Add a aggitem',
      'description' => 'Add a new aggitem',
      'page callback'  => 'aggitem_add_page',
      'access callback'  => 'aggitem_access',
      'access arguments' => array('edit'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file' => 'aggitem.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])

    );
    
    // Add menu items to add each different type of entity.
    foreach (aggitem_get_types() as $type) {
      $items[$this->path . '/add/' . $type->type] = array(
        'title' => 'Add ' . $type->label,
        'page callback' => 'aggitem_form_wrapper',
        'page arguments' => array(aggitem_create(array('type' => $type->type))),
        'access callback' => 'aggitem_access',
        'access arguments' => array('edit', 'edit ' . $type->type),
        'file' => 'aggitem.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module'])
      );
    }

    // Loading and editing aggitem entities
    $items[$this->path . '/aggitem/' . $wildcard] = array(
      'page callback' => 'aggitem_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'aggitem_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'aggitem.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    $items[$this->path . '/aggitem/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );
    
    $items[$this->path . '/aggitem/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'aggitem_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'aggitem_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'aggitem.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    
    // Menu item for viewing aggitems
    $items['aggitem/' . $wildcard] = array(
      //'title' => 'Title',
      'title callback' => 'aggitem_page_title',
      'title arguments' => array(1),
      'page callback' => 'aggitem_page_view',
      'page arguments' => array(1),
      'access callback' => 'aggitem_access',
      'access arguments' => array('view', 1),
      'type' => MENU_CALLBACK,
    );
    return $items;
  }
  
  
  /**
   * Create the markup for the add AggItem Entities page within the class
   * so it can easily be extended/overriden.
   */ 
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }    
        
    return theme('aggitem_add_list', array('content' => $content));
  }
  
}


/**
 * Form callback wrapper: create or edit a aggitem.
 *
 * @param $aggitem
 *   The aggitem object being edited by this form.
 *
 * @see aggitem_edit_form()
 */
function aggitem_form_wrapper($aggitem) {
  // Add the breadcrumb for the form's location.
  aggitem_set_breadcrumb();
  return drupal_get_form('aggitem_edit_form', $aggitem);
}


/**
 * Form callback wrapper: delete a aggitem.
 *
 * @param $aggitem
 *   The aggitem object being edited by this form.
 *
 * @see aggitem_edit_form()
 */
function aggitem_delete_form_wrapper($aggitem) {
  // Add the breadcrumb for the form's location.
  //aggitem_set_breadcrumb();
  return drupal_get_form('aggitem_delete_form', $aggitem);
}


/**
 * Form callback: create or edit a aggitem.
 *
 * @param $aggitem
 *   The aggitem object to edit or for a create form an empty aggitem object
 *     with only a aggitem type defined.
 */
function aggitem_edit_form($form, &$form_state, $aggitem) {
  // Add the default field elements.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('AggItem Name'),
    '#default_value' => isset($aggitem->name) ? $aggitem->name : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );
  
  $form['data']['#tree'] = TRUE;
  $form['data']['sample_data'] = array(
    '#type' => 'checkbox',
    '#title' => t('An interesting aggitem switch'),
    '#default_value' => isset($aggitem->data['sample_data']) ? $aggitem->data['sample_data'] : 1,
  );

  // Add the field related form elements.
  $form_state['aggitem'] = $aggitem;
  field_attach_form('aggitem', $aggitem, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save aggitem'),
    '#submit' => $submit + array('aggitem_edit_form_submit'),
  );
  
  if (!empty($aggitem->name)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete aggitem'),
      '#suffix' => l(t('Cancel'), 'admin/content/aggitems'),
      '#submit' => $submit + array('aggitem_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'aggitem_edit_form_validate';
  return $form;
}


/**
 * Form API validate callback for the aggitem form
 */
function aggitem_edit_form_validate(&$form, &$form_state) {
  $aggitem = $form_state['aggitem'];
  
  // Notify field widgets to validate their data.
  field_attach_form_validate('aggitem', $aggitem, $form, $form_state);
}


/**
 * Form API submit callback for the aggitem form.
 * 
 * @todo remove hard-coded link
 */
function aggitem_edit_form_submit(&$form, &$form_state) {
  
  $aggitem = entity_ui_controller('aggitem')->entityFormSubmitBuildEntity($form, $form_state);
  // Save the aggitem and go back to the list of aggitems
  
  // Add in created and changed times.
  if ($aggitem->is_new = isset($aggitem->is_new) ? $aggitem->is_new : 0){
    $aggitem->created = time();
  }

  $aggitem->changed = time();
  
  $aggitem->save();
  $form_state['redirect'] = 'admin/content/aggitems';
}

/**
 * Form API submit callback for the delete button.
 * 
 * @todo Remove hard-coded path
 */
function aggitem_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/content/aggitems/aggitem/' . $form_state['aggitem']->aggitem_id . '/delete';
}


/**
 * Form callback: confirmation form for deleting a aggitem.
 *
 * @param $aggitem
 *   The aggitem to delete
 *
 * @see confirm_form()
 */
function aggitem_delete_form($form, &$form_state, $aggitem) {
  $form_state['aggitem'] = $aggitem;

  $form['#submit'][] = 'aggitem_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete aggitem %name?', array('%name' => $aggitem->name)),
    'admin/content/aggitems/aggitem',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
  
  return $form;
}

/**
 * Submit callback for aggitem_delete_form
 */
function aggitem_delete_form_submit($form, &$form_state) {
  $aggitem = $form_state['aggitem'];

  aggitem_delete($aggitem);

  drupal_set_message(t('The aggitem %name has been deleted.', array('%name' => $aggitem->name)));
  watchdog('aggitem', 'Deleted aggitem %name.', array('%name' => $aggitem->name));

  $form_state['redirect'] = 'admin/content/aggitems';
}



/**
 * Page to add AggItem Entities.
 *
 * @todo Pass this through a proper theme function
 */
function aggitem_add_page() {
  $controller = entity_ui_controller('aggitem');
  return $controller->addPage();
}


/**
 * Displays the list of available aggitem types for aggitem creation.
 *
 * @ingroup themeable
 */
function theme_aggitem_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<dl class="aggitem-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer aggitem types')) {
      $output = '<p>' . t('AggItem Entities cannot be added because you have not created any aggitem types yet. Go to the <a href="@create-aggitem-type">aggitem type creation page</a> to add a new aggitem type.', array('@create-aggitem-type' => url('admin/structure/aggitem_types/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No aggitem types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}





/**
 * Sets the breadcrumb for administrative aggitem pages.
 */
function aggitem_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Content'), 'admin/content'),
    l(t('AggItems'), 'admin/content/aggitems'),
  );

  drupal_set_breadcrumb($breadcrumb);
}



