<?php

/**
 * @file
 * AggItem type editing UI.
 */

/**
 * UI controller.
 */
class AggItemTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Manage aggitem entity types, including adding
		and removing fields and the display of fields.';
    return $items;
  }
}

/**
 * Generates the aggitem type editing form.
 */
function aggitem_type_form($form, &$form_state, $aggitem_type, $op = 'edit') {

  if ($op == 'clone') {
    $aggitem_type->label .= ' (cloned)';
    $aggitem_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $aggitem_type->label,
    '#description' => t('The human-readable name of this aggitem type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($aggitem_type->type) ? $aggitem_type->type : '',
    '#maxlength' => 32,
//    '#disabled' => $aggitem_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'aggitem_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this aggitem type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['data']['#tree'] = TRUE;
  $form['data']['sample_data'] = array(
    '#type' => 'checkbox',
    '#title' => t('An interesting aggitem switch'),
    '#default_value' => !empty($aggitem_type->data['sample_data']),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save aggitem type'),
    '#weight' => 40,
  );

  //Locking not supported yet
  /*if (!$aggitem_type->isLocked() && $op != 'add') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete aggitem type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('aggitem_type_form_submit_delete')
    );
  }*/
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function aggitem_type_form_submit(&$form, &$form_state) {
  $aggitem_type = entity_ui_form_submit_build_entity($form, $form_state);
  $aggitem_type->save();
  $form_state['redirect'] = 'admin/structure/aggitem_types';
}

/**
 * Form API submit callback for the delete button.
 */
function aggitem_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/aggitem_types/manage/' . $form_state['aggitem_type']->type . '/delete';
}
