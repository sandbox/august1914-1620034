<?php

/**
 * This field handler aggregates operations that can be done on a aggitem
 * under a single field providing a more flexible way to present them in a view
 */
class aggitem_handler_aggitem_operations_field extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['aggitem_id'] = 'aggitem_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {

    $links = menu_contextual_links('aggitem', 'admin/content/aggitems/aggitem', array($this->get_value($values, 'aggitem_id')));
    
    if (!empty($links)) {
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
  }
}
