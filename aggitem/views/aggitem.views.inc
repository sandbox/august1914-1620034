<?php

/**
 * @file
 * Providing extra functionality for the AggItem UI via views.
 */


/**
 * Implements hook_views_data()
 */
function aggitem_views_data_alter(&$data) { 
  $data['aggitem']['link_model'] = array(
    'field' => array(
      'title' => t('Link'),
      'help' => t('Provide a link to the aggitem.'),
      'handler' => 'aggitem_handler_link_field',
    ),
  );
  $data['aggitem']['edit_model'] = array(
    'field' => array(
      'title' => t('Edit Link'),
      'help' => t('Provide a link to the edit form for the aggitem.'),
      'handler' => 'aggitem_handler_edit_link_field',
    ),
  );
  $data['aggitem']['delete_model'] = array(
    'field' => array(
      'title' => t('Delete Link'),
      'help' => t('Provide a link to delete the aggitem.'),
      'handler' => 'aggitem_handler_delete_link_field',
    ),
  );
  // This content of this field are decided based on the menu structure that
  // follows aggitems/aggitem/%aggitem_id/op
  $data['aggitem']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all operations available for this aggitem.'),
      'handler' => 'aggitem_handler_aggitem_operations_field',
    ),
  );
}


/**
 * Implements hook_views_default_views().
 */
function aggitem_views_default_views() {
  $views = array();

  $view = new view;
  $view->name = 'aggitems';
  $view->description = 'A list of all aggitems';
  $view->tag = 'aggitems';
  $view->base_table = 'aggitem';
  $view->human_name = 'AggItems';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'AggItems';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'create any aggitem type';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'aggitem_id' => 'aggitem_id',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'aggitem_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Empty ';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'No aggitems have been created yet';
  /* Field: AggItem: AggItem ID */
  $handler->display->display_options['fields']['aggitem_id']['id'] = 'aggitem_id';
  $handler->display->display_options['fields']['aggitem_id']['table'] = 'aggitem';
  $handler->display->display_options['fields']['aggitem_id']['field'] = 'aggitem_id';
  $handler->display->display_options['fields']['aggitem_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['aggitem_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['aggitem_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['aggitem_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['aggitem_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['aggitem_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['aggitem_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['aggitem_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['aggitem_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['aggitem_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['aggitem_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['aggitem_id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['aggitem_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['aggitem_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['aggitem_id']['empty_zero'] = 0;
  /* Field: AggItem: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'aggitem';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  /* Field: AggItem: Link */
  $handler->display->display_options['fields']['link_model']['id'] = 'link_model';
  $handler->display->display_options['fields']['link_model']['table'] = 'aggitem';
  $handler->display->display_options['fields']['link_model']['field'] = 'link_model';
  $handler->display->display_options['fields']['link_model']['label'] = 'View';
  $handler->display->display_options['fields']['link_model']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['link_model']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['link_model']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['link_model']['alter']['external'] = 0;
  $handler->display->display_options['fields']['link_model']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['link_model']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['link_model']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['link_model']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['link_model']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['link_model']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['link_model']['alter']['html'] = 0;
  $handler->display->display_options['fields']['link_model']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['link_model']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['link_model']['hide_empty'] = 0;
  $handler->display->display_options['fields']['link_model']['empty_zero'] = 0;
  /* Field: AggItem: Operations links */
  $handler->display->display_options['fields']['operations']['id'] = 'operations';
  $handler->display->display_options['fields']['operations']['table'] = 'aggitem';
  $handler->display->display_options['fields']['operations']['field'] = 'operations';
  $handler->display->display_options['fields']['operations']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['external'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['operations']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['operations']['alter']['html'] = 0;
  $handler->display->display_options['fields']['operations']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['operations']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['operations']['hide_empty'] = 0;
  $handler->display->display_options['fields']['operations']['empty_zero'] = 0;
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'models_admin_page');
  $handler->display->display_options['path'] = 'admin/content/aggitems/list';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'List';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['tab_options']['type'] = 'tab';
  $handler->display->display_options['tab_options']['title'] = 'AggItems';
  $handler->display->display_options['tab_options']['description'] = 'Manage aggitems';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';
  $translatables['aggitems'] = array(
    t('Master'),
    t('AggItems'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('Empty '),
    t('No aggitems have been created yet'),
    t('AggItem ID'),
    t('.'),
    t(','),
    t('Name'),
    t('View'),
    t('Operations links'),
    t('Page'),
  );
  $views[] = $view;
  return $views;

}
