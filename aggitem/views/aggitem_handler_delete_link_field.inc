<?php

/**
 * @file
 * Contains a Views field handler to take care of displaying deletes links
 * as fields
 */


class aggitem_handler_delete_link_field extends aggitem_handler_link_field {
  function construct() {
    parent::construct();
    $this->additional_fields['type'] = 'type';
  }


  function render($values) {
    $type = $values->{$this->aliases['type']};
    
    //Creating a dummy aggitem to check access against
    $dummy_model = (object) array('type' => $type);
    if (!aggitem_access('edit', $dummy_model)) {
      return;
    }
    
    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');
    $aggitem_id = $values->{$this->aliases['aggitem_id']};
    
    return l($text, 'admin/content/aggitems/aggitem/' . $aggitem_id . '/delete');
  }
}
